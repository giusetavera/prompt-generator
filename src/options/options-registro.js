export const optRegistroComunicativo = [
  {
    value: "formale",
    label: "Formale",
  },
  {
    value: "informale",
    label: "Informale",
  },
  {
    value: "persuasivo",
    label: "Persuasivo",
  },
];
