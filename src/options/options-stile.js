export const optStileScrittura = [
  {
    value: "divulgativo",
    label: "Divulgativo",
  },
  {
    value: "comunicativo",
    label: "Comunicativo",
  },
  {
    value: "empatico",
    label: "Empatico",
  },
  {
    value: "giornalistico",
    label: "Giornalistico",
  },
];
