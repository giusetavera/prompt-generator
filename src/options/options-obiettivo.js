export const optObiettivo = [
  {
    value: "analizzare-tendenze",
    label: "Analizzare le tendenze",
  },
  {
    value: "informare",
    label: "Informare",
  },
  {
    value: "raccontare",
    label: "Raccontare",
  },
  {
    value: "fare-divulgare",
    label: "Fare divulgazione",
  },
];
