export const optAgisci = [
  {
    value: "esperto-seo",
    label: "Esperto SEO",
  },
  {
    value: "esperto-copywriter",
    label: "Esperto Copywriter",
  },
  {
    value: "medico",
    label: "Medico",
  },
  {
    value: "blogger",
    label: "Blogger",
  },
];
