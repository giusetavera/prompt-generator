import React, { useContext, useEffect, useState } from "react";
import Label from "../../common/form/Label";
import Button from "../../common/form/Button";
import Title from "../../common/Title";
import Select from "../../common/form/Select";
import InputText from "../../common/form/InputText";
import Form from "../../common/form/Form";
import InputCheckbox from "../../common/form/InputCheckbox";
import TextArea from "../../common/form/TextArea";
import InputNumber from "../../common/form/InputNumber";
import Error from "../../common/Error";

// Options
import { optAgisci } from "../../options/options-agisci";
import { optObiettivo } from "../../options/options-obiettivo";
import { optPubblicoDestinazione } from "../../options/options-pubblico";
import { optStileScrittura } from "../../options/options-stile";
import { optSentiment } from "../../options/options-sentiment";
import { optRegistroComunicativo } from "../../options/options-registro";

// Context
import { FormContext } from "../../context/FormContext";

const PromptParams = () => {
  const { formValues, setFormValues } = useContext(FormContext);
  const [submitted, setSubmitted] = useState(false);

  const handleChange = (e) => {
    const { name, type, checked, selectedOptions, value } = e.target;

    const fieldValue =
      type === "checkbox"
        ? checked
        : type === "select-one"
        ? selectedOptions[0].text
        : value;

    setFormValues({
      ...formValues,
      [name]: fieldValue,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setSubmitted(true);

    console.log("formValues", formValues);
  };

  const handleReset = () => {
    setFormValues({});
    localStorage.removeItem("formValues");
    setSubmitted(false);
  };

  useEffect(() => {
    localStorage.setItem("formValues", JSON.stringify(formValues));
  }, [formValues]);

  return (
    <div className="prompt-params mb-10 md:mb-0">
      <Title text="Parametri" className="mb-4" />

      <Form method="post" onSubmit={handleSubmit} onReset={handleReset}>
        <div className="mb-4">
          <Label htmlFor="titolo" text="Titolo dell'articolo" />
          <InputText
            id="titoloArticolo"
            name="titoloArticolo"
            value={formValues["titoloArticolo"] || ""}
            onChange={handleChange}
            placeholder="Come si riproducono le manguste del Burundi"
          />
          {submitted && !formValues["titoloArticolo"] && (
            <Error message="This field is required" />
          )}
        </div>

        <div className="md:flex md:flex-row md:justify-between">
          <div className="mb-4 md:w-[49%]">
            <Label htmlFor="argomento" text="Argomento" />
            <InputText
              id="argomento"
              name="argomento"
              value={formValues.argomento || ""}
              placeholder="Come fanno le puzzette le manguste"
              onChange={handleChange}
            />
            {submitted && !formValues.argomento && <Error />}
          </div>

          <div className="mb-4 md:w-[49%]">
            <Label htmlFor="keyPrincipale" text="Keyword principale" />
            <InputText
              id="keyPrincipale"
              name="keyPrincipale"
              value={formValues.keyPrincipale || ""}
              placeholder="odore delle puzzette delle manguste"
              onChange={handleChange}
            />
            {submitted && !formValues.keyPrincipale && <Error />}
          </div>
        </div>

        <div className="md:flex md:flex-row md:justify-between">
          <div className="mb-4 md:w-[49%]">
            <Label htmlFor="agisciCome" text="Agisci come un" />
            <Select
              id="agisciCome"
              name="agisciCome"
              options={optAgisci}
              value={formValues["agisciCome"] || ""}
              onChange={handleChange}
            />
            {submitted && !formValues["agisciCome"] && <Error />}
          </div>

          <div className="mb-4 md:w-[49%]">
            <Label htmlFor="obiettivo" text="Obiettivo" />
            <Select
              id="obiettivo"
              name="obiettivo"
              options={optObiettivo}
              value={formValues["obiettivo"] || ""}
              onChange={handleChange}
            />
            {submitted && !formValues["obiettivo"] && <Error />}
          </div>
        </div>

        <div className="md:flex md:flex-row md:justify-between">
          <div className="mb-4 md:w-[49%]">
            <Label
              htmlFor="pubblicoDestinazione"
              text="Pubblico di destinazione"
            />
            <Select
              id="pubblicoDestinazione"
              name="pubblicoDestinazione"
              options={optPubblicoDestinazione}
              value={formValues["pubblicoDestinazione"] || ""}
              onChange={handleChange}
            />
            {submitted && !formValues["pubblicoDestinazione"] && <Error />}
          </div>

          <div className="mb-4 md:w-[49%]">
            <Label htmlFor="stileScrittura" text="Stile di scrittura" />
            <Select
              id="stileScrittura"
              name="stileScrittura"
              options={optStileScrittura}
              value={formValues["stileScrittura"] || ""}
              onChange={handleChange}
            />
            {submitted && !formValues["stileScrittura"] && <Error />}
          </div>
        </div>

        <div className="md:flex md:flex-row md:justify-between">
          <div className="mb-4 md:w-[49%]">
            <Label htmlFor="numeroH2" text="Numero di sezioni H2" />
            <InputNumber
              id="numeroH2"
              name="numeroH2"
              min="0"
              max="10"
              step="1"
              value={formValues["numeroH2"] || 0}
              onChange={handleChange}
            />
          </div>
          <div className="mb-4 md:w-[49%]">
            <Label htmlFor="numeroH3" text="Numero di sezioni H3" />
            <InputNumber
              id="numeroH3"
              name="numeroH3"
              min="0"
              max="10"
              step="1"
              value={formValues["numeroH3"] || 0}
              onChange={handleChange}
            />
          </div>
        </div>

        <div className="md:flex md:flex-row md:justify-between">
          <div className="mb-4 md:w-[49%]">
            <Label htmlFor="sentiment" text="Sentiment" />
            <Select
              id="sentiment"
              name="sentiment"
              options={optSentiment}
              value={formValues["sentiment"] || ""}
              onChange={handleChange}
            />
            {submitted && !formValues["sentiment"] && <Error />}
          </div>

          <div className="mb-4 md:w-[49%]">
            <Label
              htmlFor="registroComunicativo"
              text="Registro comunicativo"
            />
            <Select
              id="registroComunicativo"
              name="registroComunicativo"
              options={optRegistroComunicativo}
              value={formValues["registroComunicativo"] || ""}
              onChange={handleChange}
            />
            {submitted && !formValues["registroComunicativo"] && <Error />}
          </div>
        </div>

        <div className="md:flex md:flex-row justify-between">
          <div className="mb-4 md:w-[49%]">
            <div className="flex md:flex-row items-center">
              <Label
                htmlFor="includiElenchiPuntati"
                text="Includi elenchi puntati"
              />
              <InputCheckbox
                id="includi-elenchi-puntati"
                name="includiElenchiPuntati"
                className="ml-3"
                checked={formValues["includiElenchiPuntati"] || false}
                onChange={handleChange}
              />
            </div>

            <div className="flex md:flex-row items-center">
              <Label
                htmlFor="includiElenchiNumerati"
                text="Includi elenchi numerati"
              />
              <InputCheckbox
                id="includiElenchiNumerati"
                name="includiElenchiNumerati"
                className="ml-3"
                checked={formValues["includiElenchiNumerati"] || false}
                onChange={handleChange}
              />
            </div>
          </div>

          <div className="mb-4 md:w-[49%]">
            <div className="flex md:flex-row items-center">
              <Label htmlFor="keyCorrelate" text="Ott. per keyword correlate" />
              <InputCheckbox
                id="keyCorrelate"
                name="keyCorrelate"
                className="ml-3"
                checked={formValues["keyCorrelate"] || true}
                onChange={handleChange}
              />
            </div>

            <div className="flex md:flex-row items-center">
              <Label htmlFor="keyLongTail" text="Ott. per keyword long-tail" />
              <InputCheckbox
                id="keyLongTail"
                name="keyLongTail"
                className="ml-3"
                checked={formValues["keyLongTail"] || true}
                onChange={handleChange}
              />
            </div>
          </div>
        </div>

        <div className="mb-4">
          <Label htmlFor="info-extra" text="Inserisci informazioni extra" />
          <TextArea
            id="info-extra"
            name="info-extra"
            value={formValues["info-extra"] || ""}
            onChange={handleChange}
          />
        </div>

        <div className="flex flex-col md:flex-row mt-4 p-2 bg-slate-100 rounded-md">
          <Button type="submit" className="mb-4 md:mr-2 md:mb-0 md:w-[50%]">
            Genera Prompt
          </Button>

          <Button type="reset" className="md:ml-2 md:w-[50%]">
            Reset
          </Button>
        </div>
      </Form>
    </div>
  );
};

export default PromptParams;
