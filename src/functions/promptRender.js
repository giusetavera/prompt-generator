export const promptRender = ({
  titoloArticolo,
  agisciCome,
  argomento,
  pubblicoDestinazione,
  obiettivo,
  stileScrittura,
  sentiment,
  registroComunicativo,
  numeroH2,
  numeroH3,
  keyPrincipale,
}) => {
  // Prompt init.
  let prompt = "";

  // Prompt
  prompt += `Agisci come un ${agisciCome} con molti anni di esperienza nel tuo settore di riferimento. Scrivi un lungo articolo sull'argomento "${argomento}" dal titolo "${titoloArticolo}"`;

  prompt += "<br><br>";

  prompt += `L'articolo si rivolge ad un pubblico di ${pubblicoDestinazione} e ha come obiettivo finale quello di ${obiettivo}`;

  prompt += "<br><br>";

  prompt += `L'articolo avrà la seguente struttura: titolo, introduzione, ${numeroH2} sezioni H2, ${numeroH3} sezioni H3. Le sezioni H3 saranno sotto sezioni delle sezioni H2. Ricorda di formattare adeguatamente l'articolo distinguendo i titoli delle sezioni H2, H3 e formattando il titolo dell'articolo come un H1.`;

  prompt += "<br><br>";

  // More guidelines
  prompt += `Queste sono ulteriori linee guida per la scrittura e formattazione dell'articolo che devi rispettare:<br>
  - evitare un linguaggio formale e accademico<br>
  - prediligere un linguaggio colloquiale e naturale, come si userebbe in una conversazione con un amico. Utilizzare un vocabolario ricco e vario<br>
  - evitare di ripetere le stesse parole o frasi, e cercare di utilizzare sinonimi e parole alternative per esprimere le proprie idee in modo più preciso e sfumato<br>
  - variare la struttura delle frasi. Non utilizzare solo frasi semplici e composte, ma alternare periodi complessi e frasi brevi per creare un ritmo e una scorrevolezza naturale nel testo<br>
  - inserire elementi emotivi e personali. Non limitarsi a fornire informazioni, ma esprimere anche le proprie emozioni, opinioni e riflessioni personali sul tema trattato. Utilizzare figure retoriche e linguaggio figurato. Metafore, similitudini, ironia e sarcasmo possono arricchire il testo e renderlo più coinvolgente per il lettore<br>
  - mostrare, non dire. Invece di descrivere semplicemente le azioni o le emozioni di un personaggio, cercare di mostrarle attraverso i suoi dialoghi, pensieri e azioni concrete. Curare la punteggiatura e la grammatica<br>
  - evitare errori di punteggiatura che grammatica possono rendere il testo difficile da leggere e poco professionale<br>
  - rileggere e correggere il proprio lavoro. Una volta terminato il testo, rileggerlo attentamente per individuare e correggere eventuali errori o imprecisioni.`;

  prompt += "<br><br>";

  prompt += `Utilizzerai uno stile di scrittura ${stileScrittura}, un sentiment di tipo ${sentiment} e un registro comunicativo ${registroComunicativo}.`;

  prompt += `L'articolo dovrà soddisfare l'intento di ricerca sotteso nella parola chiave "${keyPrincipale}". Ricerca le parole chiave correlate alla parola chiave "${keyPrincipale}" e includile naturalmente nel testo. Ricerca le entità correlate alla parola chiave "${keyPrincipale}" e includile naturalmente nel testo.`;

  prompt += "<br><br>";

  return prompt;
};
