import React from "react";

const Label = ({ htmlFor, className, text }) => {
  const classes = `block text-md font-medium text-gray-700 ${className}`;

  return (
    <label htmlFor={htmlFor} className={classes}>
      {text}
    </label>
  );
};

export default Label;
