import React from "react";

const InputRange = ({
  id,
  name,
  value,
  onChange,
  min,
  max,
  step,
  required,
  className,
}) => {
  const classes = `block w-full mt-1 p-2 border border-gray-300 rounded-md focus:outline-none ${className}`;

  return (
    <input
      type="range"
      id={id}
      name={name}
      value={value}
      onChange={onChange}
      min={min}
      max={max}
      step={step}
      required={required}
      className={classes}
    />
  );
};

export default InputRange;
