import React, { useState } from "react";

const Select = ({
  id,
  name,
  options,
  required,
  className,
  value,
  onChange,
}) => {
  const [selectedValue, setSelectedValue] = useState(value || "");

  const classes = `block w-full mt-1 p-2 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:border-blue-500 focus:ring focus:ring-blue-500 focus:ring-opacity-50 ${className}`;

  const handleChange = (e) => {
    const selectedOption = e.target.value;
    setSelectedValue(selectedOption);
    if (onChange) {
      onChange(e);
    }
  };

  return (
    <select
      id={id}
      name={name}
      value={selectedValue}
      required={required}
      className={classes}
      onChange={handleChange}
    >
      <option value="">Seleziona un'opzione</option>
      {options.map((option, index) => (
        <option key={index} value={option.value}>
          {option.label}
        </option>
      ))}
    </select>
  );
};

export default Select;
