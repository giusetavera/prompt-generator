import React from "react";

const Error = ({ message = "This field is required" }) => {
  return <p className="text-red-500 text-sm mt-1">{message}</p>;
};

export default Error;
