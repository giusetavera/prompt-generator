import React from "react";

const Title = ({ className, text }) => {
  const classes = `text-3xl font-bold text-gray-900 ${className}`;

  return <h2 className={classes}>{text}</h2>;
};

export default Title;
